﻿namespace ILValidators.Model
{
    
    /// <summary>
    /// Bank model
    /// </summary>
    public class Bank
    {

        /// <summary>
        /// Gets or sets the bank identification code
        /// </summary>
        /// <value>
        /// The bank identification code.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the bank name.
        /// </summary>
        /// <value>
        /// The bank name.
        /// </value>
        public string? Name { get; set; }

       
    }
}
