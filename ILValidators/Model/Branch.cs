﻿namespace ILValidators.Model
{
    public class Branch
    {

        /// <summary>
        /// Gets or sets the Bank ID of the account holding branch.
        /// קוד בנק
        /// </summary>
        /// <value>
        /// The bank unique id.
        /// </value>
        public int BankId { get; set; }

        /// <summary>
        /// Gets or sets the name of the bank of the of the account holding branch.
        /// </summary>
        /// <value>
        /// The bank name.
        /// שם בנק
        /// </value>
        public string? BankName { get; set; }

        /// <summary>
        /// Gets or sets the branch ID of the of the account holding branch.
        /// מס סניף
        /// </summary>
        /// <value>
        /// The branch unique id.
        /// </value>
        public int BranchId { get; set; }

        /// <summary>
        /// Gets or sets the name of the branch of the of the account holding branch.
        /// </summary>
        /// <value>
        /// The branch name.
        /// שם סניף
        /// </value>
        public string? BranchName { get; set; }

        /// <summary>
        /// Gets or sets the branch address.
        /// כתובת סניף
        /// </summary>
        /// <value>
        /// The branch address.
        /// </value>
        public string? Address { get; set; }

        /// <summary>
        /// Gets or sets the branch zip code.
        /// מיקוד
        /// </summary>
        /// <value>
        /// The branch zip code.
        /// </value>
        public string? ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the city that the branch is located.
        /// יישוב
        /// </summary>
        /// <value>
        /// The branch city.
        /// </value>
        public string? City { get; set; }

        /// <summary>
        /// Gets or sets the branch phone number.
        /// טלפון
        /// </summary>
        /// <value>
        /// The branch phone number.
        /// </value>
        public string? Phone { get; set; }

        /// <summary>
        /// Gets or sets the branch fax number.
        /// פקס
        /// </summary>
        /// <value>
        /// The branch fax number.
        /// </value>
        public string? Fax { get; set; }

        /// <summary>
        /// Gets or sets the branch closed date.
        /// תאריך סגירה
        /// </summary>
        /// <value>
        /// The branch closed date.
        /// </value>
        public string? ClosedDate { get; set; }

        public bool? IsAccessible { get; set; }

        public string? Latitude { get; set; }

        public string? Longitude { get; set; }
    }
}
