﻿namespace ILValidators.Services
{
    using ILValidators.Extensions;
    using ILValidators.Model;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.Json;
    using System.Threading.Tasks;

    internal class BankAccountValidationService
    {

        private readonly IEnumerable<BankRule> _bankRules;
        public BankAccountValidationService()
        {
            this._bankRules = this.GetBanksRulesAsync();
        }

        /// <summary>
        /// Method that validates the args and run the custom account validation method if all args are valid.
        /// בדיקת חוקיות מספר חשבון בנק
        /// </summary>
        /// <param name="bank" type="int">The bank number - length should be a maximum of 2 numeric characters.</param>
        /// <param name="branch" type="string">The Branch number.</param>
        /// <param name="account" type="string">bank account number</param>
        /// <returns>returns 0 if OK otherwise return code (negative int)</returns>
        public BankValidationStatus IsValidAccountNumberAsync(int bank, string branch, string account)
        {
            BankValidationStatus status;

            // Check non empty input account
            if (string.IsNullOrEmpty(account))
            {
                status = BankValidationStatus.EMPTY_ACCOUNT_NUMBER;
                LogFailed(status, bank, branch, account);
                return status;
            }

            // The custom validation method accepts digits only so we have to remove any non-numeric char, the public method accepts any format of user input, eg. (xx-xxxxxx, xxx xxxxxx).
            account = account.ToDigitsOnly();

            // Parse as long so the leading zeros will be removed.
            if (!long.TryParse(account, out long accountNr))
            {
                // account number contains invalid characters
                status = BankValidationStatus.INVALID_ACCOUNT_NUMBER;
                LogFailed(status, bank, branch, account);
                return status;
            }

            // Get the bank number by its id.
            // var bankRules = await this.GetBanksRulesAsync();

            // If bank exists in supported banks enum.
            var exists = Enum.IsDefined(typeof(SupportedBanks), bank);
            if (!exists)
            {
                status = BankValidationStatus.BANK_NOT_SUPPORTED;
                LogFailed(status, bank, branch, account);
                return status;
            }

            var bankRule = this._bankRules.FirstOrDefault(x => x.Id == bank);

            // if bank exists in rules list, if not found in list => return BANK_NOT_SUPPORTED status.
            if (bankRule == null)
            {
                status = BankValidationStatus.BANK_NOT_SUPPORTED;
                LogFailed(status, bank, branch, account);
                return status;
            }

            // כדי לבדוק את מס הסניף קיים ברשימת הסניפים יש להמיר לערך מספרי
            if (!int.TryParse(branch, out int branchNr))
            {
                // if branch number is not numeric value => return INVALID_BRANCH_NUMBER status.
                status = BankValidationStatus.INVALID_BRANCH_NUMBER;
                LogFailed(status, bank, branch, account);
                return status;
            }

            // Check if branch exists on the specific bank branches list.
            if (!bankRule.Branches.Contains(branchNr))
            {
                // If bank has non empty list of closed branches and this branch number found on this list => return BRANCH_CLOSED status.
                if (bankRule.ClosedBranches != null && bankRule.ClosedBranches.Contains(branchNr))
                {
                    status = BankValidationStatus.BRANCH_CLOSED;
                }
                else
                {
                    status = BankValidationStatus.BRANCH_NOT_EXISTS;
                }
                // branch number number not found on branches list nor closed branches list.
                LogFailed(status, bank, branch, account);
                return status;
            }


            // if account number is lower than 1000 => return 'too short'.
            if (accountNr / 1000 == 0)
            {
                // מס חשבון קצר מדי
                status = BankValidationStatus.ACCOUNT_TOO_SHORT;
                LogFailed(status, bank, branch, account);
                return status;
            }

            // init the 'Account' object.
            var acount = new BankAccount
            {
                AccountNr = accountNr,
                Bank = bankRule,
                Branch = new Branch { BranchId = branchNr }
            };

            if (acount.Bank.Validator == null)
            {
                throw new ArgumentNullException("Validator is not configured for this bank (Never happened...)");
            }

            // The custom validation returns the status ('Status' enum)
            status = acount.Bank.Validator.IsValidAccountNumber(acount);

            return status;

        }

        /*
        public async Task<IEnumerable<Bank>> GetBanksAsync()
        {
            var list = await this.GetBanksRulesAsync();
            return list.Select(x => new Bank { Id = x.Id, Name = x.Name });
        }
        */

        public IEnumerable<Bank> GetBanks()
        {
            return this._bankRules.Select(x => new Bank { Id = x.Id, Name = x.Name });
        }

        // <summary>
        /// Get list of banks from json config file.
        /// I do the Deserialize manually, I prefered to work harder in order to avoid 
        /// using third any 3rd party library in this project.
        /// </summary>
        /// <returns>List of type Bank instance</returns>
        private /*async Task<*/IList<BankRule>/*>*/ GetBanksRulesAsync()
        {
            var filePath = Environment.CurrentDirectory + "\\DataFiles\\banks.json";
            // read a valid (RFC 8259) json file
            // string jsonString = await File.ReadAllTextAsync(filePath);

            string jsonString = File.ReadAllText(filePath);

            var list = new List<BankRule>();
            using JsonDocument document = JsonDocument.Parse(jsonString);
            foreach (JsonElement element in document.RootElement.EnumerateArray())
            {
                var bank = new BankRule
                {
                    Id = element.GetProperty("id").GetInt32(),
                    Name = element.GetProperty("name").ToString()
                };


                if (element.TryGetProperty("divresult", out var divresult))
                {
                    bank.DivResult = divresult.GetInt32();
                }

                foreach (var result in element.GetProperty("validresult").EnumerateArray())
                {
                    bank.ValidResult.Add(result.GetInt32());
                }

                // Get the bank validator name.
                var validator = element.GetProperty("validator").ToString();

                // Get the bank concrete validator instance.
                bank.Validator = this.GetValidator(validator);

                var branches = element.GetProperty("branches");
                foreach (var branch in branches.GetProperty("active").EnumerateArray())
                {
                    bank.Branches.Add(branch.GetInt32());
                }

                if (branches.TryGetProperty("closed", out var closed))
                {
                    foreach (var branch in closed.EnumerateArray())
                    {
                        if (bank.ClosedBranches == null)
                        {
                            bank.ClosedBranches = new List<int>();
                        }

                        bank.ClosedBranches.Add(branch.GetInt32());
                    }
                }

                if (element.TryGetProperty("validresults", out var validresults))
                {
                    if (bank.ValidBranchResults == null)
                    {
                        bank.ValidBranchResults = new Dictionary<List<int>, List<int>>();
                    }

                    foreach (var results in validresults.EnumerateArray())
                    {
                        var digitsList = new List<int>();
                        var branchesList = new List<int>();
                        foreach (var res in results.GetProperty("digit").EnumerateArray())
                        {
                            digitsList.Add(res.GetInt32());
                        }
                        foreach (var res in results.GetProperty("branches").EnumerateArray())
                        {
                            branchesList.Add(res.GetInt32());
                        }
                        bank.ValidBranchResults.Add(digitsList, branchesList);
                    }
                }
                list.Add(bank);

            }

            return list;


        }

        /// <summary>
        /// Get list of branches by bank nr
        /// </summary>
        /// <param name="bank"></param>
        /// <returns></returns>
        public async Task<List<Branch>> GetBranchesByBankAsync(SupportedBanks bank)
        {
            var branches = await this.GetBranchesAsync();

            if (branches.TryGetValue((int)bank, out List<Branch>? bankBranches))
            {
                return bankBranches;
            }
            // The input bank id is not supported.
            throw new ArgumentOutOfRangeException("bank");
        }

        /// <summary>
        /// Get the branch details.
        /// </summary>
        /// <param name="bank" type="int">The bank number - length should be a maximum of 2 numeric characters.</param>
        /// <param name="branch" type="string">The Branch number.</param>
        /// <returns>Instance of Branch or null.</returns>
        public async Task<Branch?> GetBranchDetailsAsync(int bank, string branch)
        {
            Branch? bankBranch = null;
            if (!int.TryParse(branch, out int branchNr))
            {
                // There's no error handling => the method will return null;
                return bankBranch;
            }

            var bankEnum = (SupportedBanks)bank;
            if (bankEnum != SupportedBanks.Doar)
            {
                // Find the specifig branch object by branch id + bank id.
                var allBranches = await this.GetBranchesByBankAsync(bankEnum);
                bankBranch = allBranches.First(x => x.BranchId == branchNr);
            }
            else
            {
                // בנק ישראל לא מספק מידע על סניפי בנק הדואר (9) אז נחזיר מופע של מחלקת סניף ללא מידע
                bankBranch = new Branch { BranchId = branchNr };
            }
            return bankBranch;

        }

        private async Task<IDictionary<int, List<Branch>>> GetBranchesAsync()
        {
            var branchesDictionary = new Dictionary<int, List<Branch>>();
            var helper = new CSVHelper();
            // קובץ להורדה מבנק ישראל
            var filePath = Environment.CurrentDirectory + "\\DataFiles\\snifim_he.csv";
            var csv = await helper.CsvFileTextToList(filePath);
            foreach (string[] line in csv.Skip(1))
            {
                try
                {
                    var bankId = int.Parse(line[0]);
                    if (!branchesDictionary.ContainsKey(bankId)) {
                        branchesDictionary[bankId] = new List<Branch>();
                    }

                    var branch = new Branch
                    {
                        BankId = bankId,
                        BranchId = int.Parse(line[2]),
                        BranchName = line[3],
                        Address = line[4],
                        City = line[5],
                        ZipCode = line[6],
                        // PoBox = line[7],
                        Phone = line[8],
                        Fax = line[9],
                        IsAccessible = line[11] == "כן",
                        ClosedDate = line[15],
                        Latitude = line[18],
                        Longitude = line[19],
                    };

                    branchesDictionary[bankId].Add(branch);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw;
                }
            }
            return branchesDictionary;
        }

        private IValidator GetValidator(string validator)
        {
            if (string.IsNullOrEmpty(validator))
            {
                throw new NullReferenceException("validator");
            }

            var type = Type.GetType($"ILValidators.Validators.{validator}");

            if (null == type)
            {
                throw new ApplicationException("Validator not found.");
            }

            IValidator? validatorStrategy;

            try
            {
                validatorStrategy = (IValidator?)Activator.CreateInstance(type, true);
                if (null == validatorStrategy)
                {
                    throw new ApplicationException("Cannot convert validator to IValidator.");
                }
            }
            catch
            {
                throw new ApplicationException("Cannot initialize validator class.");
            }
            return validatorStrategy;
        }

        /// <summary>
        /// Log failed accounts.
        /// </summary>
        /// <param name="status" type="Status">The error status code.</param>
        /// <param name="bank" type="int">The bank number.</param>
        /// <param name="branch" type="string">The branch number.</param>
        /// <param name="account" type="string">The account number.</param>
        private static void LogFailed(BankValidationStatus status, int bank, string branch, string account)
        {
            Console.WriteLine($"status={status}; bank={bank}; branch={branch}; account={account}");
        }

    }
}
