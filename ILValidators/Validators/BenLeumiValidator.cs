﻿namespace ILValidators.Validators
{

    #region Using directives

    using System;
    using System.Linq;
    using Model;
    

    #endregion Using directives


    /// <summary>
    ///  The BenLeumi group 'ConcreteStrategy' class, inherits the <see cref="IValidator"/> interface.
    /// </summary>
    internal class BenLeumiValidator : IValidator
    {

        #region IValidator Members


        /// <summary>
        /// בדיקת חוקיות מספר חשבון בנק - קבוצת הבינלאומי
        /// הבדיקה זהה עבור:
        /// <list type="table">
        /// <listheader>
        /// <term>מס' בנק</term>
        /// <description>שם בנק</description>
        /// </listheader>
        /// <item><term>31</term><description>הבינלאומי</description></item>
        /// <item><term>52</term><description>פאג&quot;י</description></item>
        /// </list>
        /// </summary>
        /// <param name="account" type="AccountInfo">The account info</param>
        /// <returns>returns <see cref="Status"/> enum</returns>
        public BankValidationStatus IsValidAccountNumber(BankAccount account)
        {

            if (account == null || account.Bank == null || account.Branch == null)
            {
                throw new ArgumentNullException("account");
            }

            /*=========================================נסיון ראשון=========================================*/

            // משתנים A B C D E F G H - X
            var accountNr = account.AccountNr;

            // משתנים D E F G H -X
            var shortAccountNumber = accountNr % 1000000;
            
            var n = 1;
            var sum = 0;

            var loop = account.AccountNr > shortAccountNumber ? 10 : 7;

            // Counting from the rightmost digit, and moving left.
            while (n < loop)
            {
                var dig = Convert.ToInt32(accountNr % 10);
                sum += (dig * (n));
                accountNr /= 10;
                n++;
            }

            // חובה לוודא שמוגדר ערך שארית
            if (account != null && account.Bank.DivResult.HasValue)
            {
                var result = sum % account.Bank.DivResult.Value;

                // בדיקת שארית
                if (account.Bank.ValidResult.Contains(result))
                {
                    return BankValidationStatus.SUCCESS;
                }

                /*=========================================נסיון שני=========================================*/

                //  חשבונות שנמצאו שגויים לפי חישוב זה יעברו לשלב ב' בו תתבצע בדיקה נוספת זהה לראשונה אולם רק על 6 הספרות הימניות של מספר החשבון 

                // Run only once - if 'loop' equals to 10 it means that the account number has more than 6 digits (greater than 'shortAccountNumber').
                if (loop == 10)
                {
                    account.AccountNr = shortAccountNumber;
                    return IsValidAccountNumber(account);
                }
            }
                

            // חשבון שימצא שגוי גם בבדיקה של שלב ב' יחשב סופית כשגוי.
            return BankValidationStatus.FAILED;

        }

        #endregion

    }
}
