﻿namespace ILValidators.Validators
{

    #region Using directives

    using System;
    using System.Linq;
    using Model;


    #endregion Using directives

    /// <summary>
    ///  The Discount group 'ConcreteStrategy' class, inherits the <see cref="IValidator"/> interface.
    /// </summary>
    internal class DiscountValidator : IValidator
    {

        #region IValidator Members

        /// <summary>
        /// בדיקת חוקיות מספר חשבון בנק - קבוצת דיסקונט
        /// הבדיקה זהה עבור:
        /// <list type="table">
        /// <listheader>
        /// <term>מס' בנק</term>
        /// <description>שם בנק</description>
        /// </listheader>
        /// <item><term>09</term><description>בנק הדואר</description></item>
        /// <item><term>11</term><description>דיסקונט</description></item>
        /// <item><term>17</term><description>מרכנתיל-דיסקונט</description></item>
        /// </list>
        /// </summary>
        /// <param name="account" type="AccountInfo">The account info</param>
        /// <returns>returns <see cref="Status"/> enum</returns>
        public BankValidationStatus IsValidAccountNumber(BankAccount account)
        {
            if (account == null || account.Bank == null || account.Branch == null)
            {
                throw new ArgumentNullException("account");
            }

            // משתנים של קבוצת פועלים A B C D E F G H - X
            // משתנים של בנק הדואר I H G F E D C B A
            // למרות השוני בשמות המשתנים - ההכפלה זהה לחלוטין
            var accountNr = account.AccountNr;

            // יש לבצע בדיקה זו רק עבור בנק הדואר
            if ((account.Bank.Id == 9) && (accountNr / 10000000 == 0))
            {
                return BankValidationStatus.ACCOUNT_TOO_SHORT;
            }

            accountNr %= 1000000000;
            var n = 1;
            var sum = 0;
            // Counting from the rightmost digit, and moving left.
            while (n < 10)
            {
                var dig = Convert.ToInt32(accountNr % 10);
                sum += (dig * (n));
                accountNr /= 10;
                n++;
            }

            // חובה לוודא שמוגדר ערך שארית
            if (account.Bank.DivResult.HasValue)
            {
                var result = sum % account.Bank.DivResult.Value;

                // בדיקת שארית
                if (account.Bank.ValidResult.Contains(result))
                {
                    return BankValidationStatus.SUCCESS;
                }
            }

               

            // כל שארית אחרת אינה חוקית.
            return BankValidationStatus.FAILED;

        }

        #endregion

    }
}
