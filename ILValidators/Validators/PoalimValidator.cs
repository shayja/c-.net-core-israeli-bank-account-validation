﻿namespace ILValidators.Validators
{

    #region Using directives

    using System;
    using System.Linq;
    using Model;

    #endregion Using directives

    /// <summary>
    ///  The Poalim group 'ConcreteStrategy' class, inherits the <see cref="IValidator"/> interface.
    /// </summary>
    internal class PoalimValidator : IValidator
    {
        

        #region IValidator Members


        /// <summary>
        /// בדיקת חוקיות מספר חשבון בנק - קבוצת פועלים
        /// <list type="table">
        /// <listheader>
        /// <term>מס' בנק</term>
        /// <description>שם בנק</description>
        /// </listheader>
        /// <item><term>04</term><description>בנק יהב</description></item>
        /// <item><term>12</term><description>בנק הפועלים</description></item>
        /// <item><term>20</term><description>מזרחי טפחות</description></item>
        /// </list>
        /// </summary>
        /// <param name="account" type="AccountInfo">The account info</param>
        /// <returns>returns <see cref="Status"/> enum</returns>
        public BankValidationStatus IsValidAccountNumber(BankAccount account)
        {

            if (account == null || account.Bank == null || account.Branch == null) {
                throw new ArgumentNullException("account");
            }

            // מס' חשבון בבנק הפועלים מורכב מ-5 ספרות + ספרת בקורת
            // משתנים D E F G H - X
            var accountNr = account.AccountNr % 1000000;

            // משתנים S T U
            var branchNr = account.Branch.BranchId;

            // תנאי ייחודי לבנק המזרחי: לכל מספר סניף עם ערך גדול מ-400 יש להחסיר 400 לצורך החישוב
            if (account.Bank.Id == 20/*המזרחי*/ && branchNr > 400)
            {
                branchNr -= 400;
            }

            var n = 1;
            var sum = 0;
            int dig;

            // Counting from the rightmost digit, and moving left.
            while (n<7)
            {
                dig = Convert.ToInt32(accountNr % 10);
                sum += (dig*(n));
                accountNr /= 10;
                n++;
            }

            while (n < 10)
            {
                dig = branchNr % 10;
                sum += (dig * (n));
                branchNr /= 10;
                n++;
            }

            // חובה לוודא שמוגדר ערך שארית
            if (account.Bank.DivResult.HasValue)
            {
                // יש לחלק את התוצאה של ההכפלה בשארית שהוגדרה לבנק
                var result = sum % account.Bank.DivResult.Value;

                // בדיקת שארית
                if (account.Bank.ValidResult.Contains(result))
                {
                    return BankValidationStatus.SUCCESS;
                }

                // חוקיות הבדיקה תלויה בשארית + מספר הסניף כך שבכל קבוצת סניפים חוקיות השארית משתנה 
                if (account.Bank.ValidBranchResults != null)
                {
                    branchNr = account.Branch.BranchId;
                    if (account.Bank.ValidBranchResults.Any(branchResult => branchResult.Key.Contains(result) && branchResult.Value.Contains(branchNr)))
                    {
                        return BankValidationStatus.SUCCESS;
                    }
                }
            }

               

            // כל שארית אחרת אינה חוקית.
            return BankValidationStatus.FAILED;
        }

        #endregion

 

    }
}
